import $ from "jquery";
import 'popper.js';
import 'bootstrap';

(function () {

  'use strict';

  /**
   * Allow for the top-level links to resolve to their respective hrefs and
   * open the dropdown menus on hover.
   *
   * @todo Review to see if there's a better way to this.
   */
  Drupal.behaviors.navHover = {
    attach: function(context) {
      $(".block--mainnavigation .navbar-nav > .nav-item > a.nav-link, .block--secondarynavigation ul > .nav-item > a.nav-link").on('click', function(event) {
        var link_to = $(this).attr("href");
        var link_target = $(this).attr("target") || "_self";
        window.open(link_to, link_target);
      });


      function toggleDropdown(e) {
        const _d = $(e.target).closest(".dropdown"),
          _m = $(".dropdown-menu", _d);
        setTimeout(
          function() {
            const shouldOpen = e.type !== "click" && _d.is(":hover");
            _m.toggleClass("show", shouldOpen);
            _d.toggleClass("show", shouldOpen);
            $('[data-toggle="dropdown"]', _d).attr("aria-expanded", shouldOpen);
          },
          e.type === "mouseleave" ? 0 : 0
        );
      }

      $(window).on("load resize", function(e) {
        const desktopBreakpoint = parseInt(getComputedStyle(document.documentElement).getPropertyValue("--breakpoint-desktop-min"));
        if ($(window).width() >= desktopBreakpoint) {
          $("body").on("mouseenter mouseleave", ".dropdown", toggleDropdown);
        }
      });
    }
  };

  Drupal.behaviors.externalLinks = {
    attach: function(context, settings) {
      $('a[href^="http"]').addClass('external').attr('target', '_blank');
      $('a[href^="mailto"]').addClass('external').attr('target', '_blank');
      $('a[href^="/sites/default/files/"]').attr('download', '');
    }
  };

  Drupal.behaviors.dropdownWrap = {
    attach: function(context, settings) {
      $(".dropdown-item", context).wrapInner("<span></span>");
      $(".block--secondarynavigation .nav-link", context).wrapInner(
        "<span></span>"
      );
    }
  };

  Drupal.behaviors.searchformButtonSm = {
    attach: function(context, settings) {
      $(once('searchformButtonSm', '.block--searchform-sm .form-submit'))
        .each(function() {
          $(this).after(
            '<button class="search-close"><span class="sr-only">Close search form</span></button>'
          );
        });
    }
  };

  Drupal.behaviors.searchformButtonMd = {
    attach: function(context, settings) {
      $(once('searchformButtonMd', '.block--searchform-md .form-submit'))
        .each(function() {
          $(this).after('<button class="search-close">Cancel</button>');
        });
    }
  };

  Drupal.behaviors.searchformPlaceholder = {
    attach: function(context, settings) {

      $(once('searchformPlaceholder', '.block--searchform-sm .form-search, .block--searchform-md .form-search'))

        .each(function() {
          $(this).attr(
            "placeholder",
            "Type a word or phrase to begin your search..."
          );
        });
    }
  };

  Drupal.behaviors.searchformFocus = {
    attach: function(context, settings) {
      $(function() {
        const input = $(".block--searchform .form-search");
        if ($(input).length) {
          $(input).focus(function() {
            $(input)
              .closest("form").addClass("focused");

          });
          $(input).blur(function() {
            $(input)
              .closest("form").removeClass("focused");
          });
        }
      });
    }
  };

  Drupal.behaviors.searchformClose = {
    attach: function(context, settings) {
      $(".search-close", context).click(function() {
        $(this)
          .closest(".collapse")
          .removeClass("show");
        event.preventDefault();
      });
    }
  };

  Drupal.behaviors.menuToggles = {
    attach: function(context, settings) {
      $(once('menuToggles', '.block--mainnavigation-sm .dropdown-toggle, .block--mainnavigation-md .dropdown-toggle'))
        .each(function() {
          $(this).removeAttr("data-toggle");
          $(this).after(
            '<button class="menu--toggle" data-toggle="dropdown"><span class="sr-only">Close menu</span></button>'
          );
        });
    }
  };

  Drupal.behaviors.offcanvasNavCloseButton = {
    attach: function(context, settings) {
      $(once('offcanvasNavClose', '#offcanvas-side .offcanvas__container'))
        .each(function() {
          $(this).before(
            '<button class="offcanvas--close offcanvas-side--close">Close</button>'
          );
        });
    }
  };

  Drupal.behaviors.offcanvasClose = {
    attach: function(context, settings) {
      $(".offcanvas--close", context).click(function() {
        $(this)
          .closest(".collapse")
          .removeClass("show");
        $("[data-target='#offcanvas-side']").attr("aria-expanded", false);
      });
    }
  };

  Drupal.behaviors.searchPageTitle = {
    attach: function(context, settings) {
      $(once('searchPageTitle', '.path-search h1'))
        .each(function() {
          var text = $(this).text();
          text = text.replace("Search for", "Search Results for");
          $(this).text(text).css({"display": "block"});
        });
    }
  };

  Drupal.behaviors.chosenAccessibilityFix = {
    attach: function (context, settings) {
      $("form").each(function (index, element) {
        $('body').on('chosen:ready', function(evt, params) {
          $('.js-form-item.js-form-type-select', context).each(function(index, element) {
            $(this).find('.chosen-container input.chosen-search-input').attr('aria-label', $(this).find('label').text().trim());
          });
        });
      });
    }
  };

})(jQuery, Drupal);
