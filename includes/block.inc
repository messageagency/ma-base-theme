<?php

/**
 * @file
 * Theme and preprocess functions for blocks.
 */

/**
 * Implements hook_theme_suggestions_HOOK_alter() for blocks.
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_theme_suggestions_block_alter(array &$suggestions, array $variables) {
// @codingStandardsIgnoreEnd
  // Don't include block wrapper/title markup for these blocks.
  $bare_blocks = [];
  if (in_array($variables['elements']['#plugin_id'], $bare_blocks)) {
    $suggestions[] = 'block__bare';
  }
}

/**
 * Implements hook_preprocess_block() for blocks.
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_preprocess_block(&$variables) {
// @codingStandardsIgnoreEnd
  if (isset($variables['id'])) {
    if ($variables['id'] == 'mainnavigation') {
      $variables['utility_classes'] = ['w-100'];
    }
  }
}
