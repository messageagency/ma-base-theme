<?php

/**
 * @file
 * Custom theme hooks.
 */

use Drupal\Core\Asset\AttachedAssetsInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Implements hook_js_alter().
 *
 * Makes sure Chosen JS loads after site theme JS.
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_js_alter(&$javascript, AttachedAssetsInterface $assets, LanguageInterface $language) {
// @codingStandardsIgnoreEnd
  $my_theme_js = 'web/themes/custom/RADIX_SUBTHEME_MACHINE_NAME/assets/js/RADIX_SUBTHEME_MACHINE_NAME.script.js';
  if (isset($javascript[$my_theme_js]) && isset($javascript['web/modules/contrib/chosen/js/chosen.js'])) {
    $javascript['web/modules/contrib/chosen/js/chosen.js']['weight'] = $javascript[$my_theme_js]['weight'] + 10000;
    $javascript['web/modules/contrib/chosen/js/chosen.js']['group'] = $javascript[$my_theme_js]['group'];
  }
}
