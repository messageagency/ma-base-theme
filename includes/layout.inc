<?php

/**
 * @file
 * Theme and preprocess functions for layouts.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\ma_sprout_helpers\Helpers\SproutHelpers;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Implements hook_preprocess_HOOK() for template card.html.twig.
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_preprocess_card(&$variables) {
// @codingStandardsIgnoreEnd
  if (!isset($variables['content'])) {
    return;
  }
  $content = $variables['content'];
  if (!isset($content['#entity'])) {
    return;
  }
  $entity = $content['#entity'];

  // Card type.
  $card_type = '';
  $classes = $variables['settings']['layout']['classes'];
  foreach ($classes as $class) {
    $card_type = (strpos($class, 'card-img-') !== FALSE) ? $class : $card_type;
  }
  $variables['card_type'] = $card_type;

  // Display type.
  $variables['display_type'] = 'display-' . $content['#view_mode'];

  // Title.
  // @todo This is mostly here as an example.
  switch ($content['#view_mode']) {
    case 'teaser':
    case 'featured':
      $variables['heading_utilities'][] = 'h3';
      break;
  }

  // Has media.
  // @todo This assumes media fields will always be in the card_image region.
  // @todo Might be better to check all fields in the display.
  if (isset($content['card_image'])) {
    foreach ($content['card_image'] as $field_name => $field_settings) {
      if (strpos($field_name, 'field_') === FALSE) {
        continue;
      }
      $variables['has_media'] = RADIX_SUBTHEME_MACHINE_NAME_has_media($entity, $field_name);
      $variables['has_media'] = ($variables['has_media'] == 'fontawesome_icon') ? 'icon' : $variables['has_media'];
      if ($variables['has_media']) {
        break;
      }
    }
  }

  // Left or right image modifications.
  if ($card_type == 'card-img-left' || $card_type == 'card-img-right') {
    if (!isset($variables['has_media']) || !$variables['has_media']) {
      foreach ($variables['settings']['regions']['card_body']['classes'] as $class) {
        if (strpos($class, 'col-') === 0) {
          unset($variables['settings']['regions']['card_body']['classes'][$class]);
        }
        $variables['settings']['regions']['card_body']['classes']['col'] = 'col';
      }
    }
  }
  // @todo any non-image field in the image region should
  // @todo receive the class .card-img-overlay. maybe?
  if ($entity instanceof EntityInterface) {
    switch ($entity->bundle()) {
      // Signpost bands.
      case 'signpost':
        if (isset($content['field_signpost_link'][0])) {
          $variables['url'] = $content['field_signpost_link'][0]['#url']->toString();
          $variables['target'] = (isset($content['field_signpost_link'][0]['#options']['external']) &&
            $content['field_signpost_link'][0]['#options']['external'] == TRUE) ? '_blank' : '_self';
        }
        $variables['card_type'] = 'card-img-top';
        break;

      case 'accordion_item':
        $variables['contained'] = TRUE;
        $variables['card_type'] = 'card-img-left';
        $variables['parent_id'] = ($entity->hasField('parent_id') && !$entity->parent_id->isEmpty()) ? $entity->parent_id->value : '';
        $variables['target'] = 'accordion-item-' . $entity->id();
        break;
    }
  }

  // Image position.
  switch ($card_type) {
    case 'card-img-top':
      $variables['image_position'] = 'top';
      break;

    case 'card-img-left':
      $variables['image_position'] = 'left';
      break;

    case 'card-img-right':
      $variables['image_position'] = 'right';
      break;

    case 'card-img-bg':
      $variables['image_position'] = 'background';
      break;

    default:
      $variables['image_position'] = '';
      break;
  }
}

/**
 * Implements hook_preprocess_HOOK() for template standard.html.twig.
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_preprocess_standard(&$variables) {
// @codingStandardsIgnoreEnd
  // Only render a single subnav.
  SproutHelpers::SingleField($variables['content'], 'dynamic_block_field:node-subnav', 'dynamic_block_field:node-subnav_secondary');
}

/**
 * Implements hook_preprocess_HOOK() for template band.html.twig.
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_preprocess_band(&$variables) {
// @codingStandardsIgnoreEnd
  $content = $variables['content'];
  $entity = $content['#entity'];

  if ($entity instanceof Paragraph) {
    // Paragraph ID.
    $variables['paragraph_id'] = $entity->id();
    $variables['attributes']['id'] = "band-" . $entity->id();

    // Check for media.
    // @todo We should not assume the field name here.
    // @todo has_media() should look at each field used in the display.
    $variables['has_media'] = RADIX_SUBTHEME_MACHINE_NAME_has_media($entity, 'field_pg_media');
    $variables['has_media'] = ($variables['has_media'] == 'remote_video') ? 'video' : $variables['has_media'];

    // Background color.
    if ($entity->hasField('field_pg_bg_color') && isset($entity->field_pg_bg_color->value)) {
      $color = str_replace('_', '-', $entity->field_pg_bg_color->value);
      $variables['bg_color'] = 'bg-' . $color;
    }

    // Boxed.
    if ($entity->hasField('field_pg_boxed') && isset($entity->field_pg_boxed->value)) {
      $variables['boxed'] = $entity->field_pg_boxed->value;
    }

    // Alignment.
    if ($entity->hasField('field_pg_alignment') && isset($entity->field_pg_alignment->value)) {
      $variables['alignment'] = $entity->field_pg_alignment->value;
    }

    switch ($entity->bundle()) {
      // Content grid bands.
      case 'content_grid':
        // @todo Check that content_items are in fact cards.
        $variables['content_items'] = RADIX_SUBTHEME_MACHINE_NAME_get_field_items_render_arrays($content, 'field_pg_content');
        break;

      // Signpost grid bands.
      case 'signpost_grid':
        $variables['signposts'] = RADIX_SUBTHEME_MACHINE_NAME_get_field_items_render_arrays($content, 'field_pg_signposts');
        break;

      // Accordion bands.
      case 'accordion':
        $variables['accordion_items'] = RADIX_SUBTHEME_MACHINE_NAME_get_field_items_render_arrays($content, 'field_pg_accordion_items');
        break;

      // Menu Grid bands.
      case 'menu_grid':
        // @todo Make menu items render elements?
        SproutHelpers::SingleField($variables['content'], 'dynamic_block_field:paragraph-grid_nav', 'dynamic_block_field:paragraph-grid_nav_secondary');
        $main_nav = isset($variables['content']['dynamic_block_field:paragraph-grid_nav']);
        $secondary_nav = isset($variables['content']['dynamic_block_field:paragraph-grid_nav_secondary']);
        if (!$main_nav && !$secondary_nav) {
          $variables['no_build'] = TRUE;
        }
        break;

      // Story bands.
      case 'story':
        // If no media, remove any col- classes.
        if (!$variables['has_media']) {
          foreach ($variables['settings']['regions']['band_content']['classes'] as $class) {
            if (strpos($class, 'col-') === 0) {
              unset($variables['settings']['regions']['band_content']['classes'][$class]);
            }
          }
        }

        // Check media position and apply appropriate class.
        $variables['image_position'] = 'left';
        if ($entity->hasField('field_pg_image_position') && isset($entity->field_pg_image_position->value)) {
          $variables['image_position'] = $entity->field_pg_image_position->value;
        }
        if ($variables['image_position'] == 'right') {
          $variables['settings']['regions']['band_media']['classes']['order-md-last'] = 'order-md-last';
          $variables['settings']['regions']['band_content']['classes']['order-md-first'] = 'order-md-first';
        }
        break;
    }
  }
}

/**
 * Check entity for media.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   Entity.
 * @param string $field_name
 *   Field.
 *
 * @return string
 *   media type or NULL.
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_has_media(EntityInterface $entity, string $field_name) {
// @codingStandardsIgnoreEnd
  $has_media = NULL;
  if ($entity->hasField($field_name)) {
    /** @var \Drupal\field\Entity\FieldConfig $field */
    $field = $entity->getFieldDefinition($field_name);
    $field_type = $field->getType();

    if ($field_type == 'entity_reference' && $field->getSetting('target_type') == 'media') {
      /** @var \Drupal\media\Entity\Media $media */
      $media = $entity->$field_name->isEmpty() ? NULL : $entity->$field_name->entity;
      $has_media = $media ? $media->bundle() : NULL;
    }
    elseif ($field_type == 'image' ||
      $field_type == 'video_embed_field' ||
      $field_type == 'signpost_icon' ||
      $field_type == 'fontawesome_icon') {
      $has_media = $entity->$field_name->isEmpty() ? NULL : $field_type;
    }
  }
  return $has_media;
}

/**
 * Get Render arrays for each item from a field.
 *
 * @param array $content
 *   The content render array from an entity.
 * @param string $field_name
 *   The field from which to get the items.
 *
 * @return array
 *   An array of renderable field items.
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_get_field_items_render_arrays(array $content, string $field_name) {
// @codingStandardsIgnoreEnd
  $items = [];
  if (isset($content[$field_name])) {
    foreach ($content[$field_name] as $key => $value) {
      if (!is_int($key)) {
        continue;
      }
      $items[] = $value;
    }
  }
  return $items;
}
