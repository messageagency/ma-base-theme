<?php

/**
 * @file
 * Theme and preprocess functions for images.
 */

/**
 * Implements hook_preprocess_image().
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_preprocess_image(array &$variables) {
// @codingStandardsIgnoreEnd
  if (isset($variables['style_name'])) {
    $variables['attributes']['class'][] = 'img-style--' . $variables['style_name'];
  }
}
