<?php

/**
 * @file
 * Theme and preprocess functions for html.
 */

use Drupal\Component\Utility\Html;

/**
 * Implements template_preprocess_html().
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_preprocess_html(&$variables) {
// @codingStandardsIgnoreEnd
  // Add path fragments to body classes.
  $path = array_filter(explode('/', \Drupal::request()->getPathInfo()));
  $last_part = '';
  foreach ($path as $part) {
    $part = is_numeric($part) ? '_' : Html::cleanCssIdentifier($part);

    if (!empty($last_part)) {
      $part = $last_part . '-' . $part;
    }

    $variables['attributes']['class'][] = 'path-' . $part;
    $last_part = $part;
  }

  // Add -end marker.
  $variables['attributes']['class'][] = 'path-' . $last_part . '-end';

  // Add node id to body classes.
  if (isset($variables['node_type'])) {
    $node = \Drupal::routeMatch()->getParameter('node');
    // Add node-id class to the element.
    $variables['node_id'] = $node->id();
  }

  // Add term id and vocab name to body classes.
  if (\Drupal::routeMatch()->getRouteName() == 'entity.taxonomy_term.canonical') {
    // Load the term entity and get the data from there.
    $term = \Drupal::routeMatch()->getParameter('taxonomy_term');
    $term_id = $term->id();
    $vocabulary_name = $term->bundle();
    $variables['term_id'] = $term_id;
    $variables['vocabulary_name'] = $vocabulary_name;
  }

  // Add preconnect to speed up font loading.
  // This can be removed if Google fonts are not used.
  $preconnect = [
    '#tag' => 'link',
    '#attributes' => [
      'rel' => 'preconnect',
      'href' => 'https://fonts.gstatic.com',
      'crossorigin' => 'anonymous',
    ],
  ];

  $variables['page']['#attached']['html_head'][] = [$preconnect, 'preconnect'];
}
