<?php

/**
 * @file
 * Theme and preprocess functions for menus.
 */

use Drupal\ma_sprout_helpers\Helpers\SproutHelpers;
use Drupal\node\Entity\Node;

/**
 * Implements hook_preprocess_HOOK() for template menu.html.twig.
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_preprocess_menu(&$variables) {
// @codingStandardsIgnoreEnd
  if (isset($variables['menu_block_configuration'])) {
    if (isset($variables['menu_block_configuration']['suggestion'])) {
      switch ($variables['menu_block_configuration']['suggestion']) {
        case 'gridnav':
          foreach ($variables['items'] as $key => $menulink) {
            /** @var \Drupal\Core\Url $url */
            $url = $menulink['url'] ?? NULL;
            if (!$url) {
              $variables['items'][$key]['path'] = NULL;
              continue;
            }
            $variables['items'][$key]['path'] = $url->toString();
            $params = $menulink['original_link']->getRouteParameters();
            if (!isset($params['node'])) {
              $variables['items'][$key]['img_info'] = [];
              continue;
            }
            $node = Node::load($params['node']);
            // @todo Let's not hardcode the field name and image style.
            $img_info = SproutHelpers::getImageInfo($node, 'field_media', 'tile');
            if (empty($img_info)) {
              $img_info = SproutHelpers::getImageInfo($node, 'field_image', 'tile');
            }
            $variables['items'][$key]['img_info'] = $img_info;
          }
          break;
      }
    }
  }
}
