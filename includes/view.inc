<?php

/**
 * @file
 * Theme and preprocess functions for views.
 */

/**
 * Implements hook_preprocess_HOOK() for template views-view-grid.html.twig.
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_preprocess_views_view_grid(&$variables) {
// @codingStandardsIgnoreEnd
  /** @var \Drupal\views\ViewExecutable $view */
  $view = $variables['view'];
  $variables['css_class'] = $view->getDisplay()->getOption('css_class');
  // Default container_type is deck.
  // @see components/03-organisms/card/card-container.twig.
  // @see https://getbootstrap.com/docs/4.0/components/card/#card-layout.
  $types = [
    'deck',
    'group',
    'columns',
  ];
  $variables['container_type'] = '';
  foreach ($types as $type) {
    if (strpos($variables['css_class'], $type) !== FALSE) {
      $variables['container_type'] = $type;
      break;
    }
  }
}
