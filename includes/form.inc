<?php

/**
 * @file
 * Theme and preprocess functions for forms.
 */

/**
 * Implements hook_theme_suggestions_HOOK().
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_theme_suggestions_input_alter(&$suggestions, array $variables) {
// @codingStandardsIgnoreEnd
  $element = $variables['element'];

  if (isset($element['#attributes']['data-twig-suggestion'])) {
    $suggestions[] = 'input__' . $element['#type'] . '__' . $element['#attributes']['data-twig-suggestion'];
  }
}

/**
 * Implements hook_preprocess_form_element().
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_preprocess_form_element(&$variables) {
// @codingStandardsIgnoreEnd
  if ($variables['type'] == 'search') {
    if (isset($variables['name']) && $variables['name'] == 'keys') {
      // I don't understand why this isn't set in the first place.
      $variables['label']['#title'] = 'Search';
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_form_search_block_form_alter(&$form, &$form_state) {
// @codingStandardsIgnoreEnd
  $form['keys']['#attributes']['placeholder'][] = t('Search');
  $form['actions']['submit']['#attributes']['data-twig-suggestion'] = 'search_submit';
  $form['actions']['submit']['#attributes']['class'][] = 'search__submit';
}

/**
 * Implements template_preprocess_item_list__search_results().
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_preprocess_item_list__search_results(&$variables) {
// @codingStandardsIgnoreEnd
  foreach ($variables['items'] as $key => $item) {
    $node = $item['value']['#result']['node'] ?? NULL;
    if ($node) {
      $variables['items'][$key]['value']['#result']['node_teaser'] = \Drupal::entityTypeManager()->getViewBuilder('node')->view($node, 'teaser');
    }
  }
}
