<?php

/**
 * @file
 * Theme and preprocess functions for fields.
 */

/**
 * Implements hook_theme_suggestions_HOOK_alter() for fields.
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_theme_suggestions_field_alter(array &$suggestions, array $variables) {
// @codingStandardsIgnoreEnd
  if (isset($variables['element'])) {
    if (isset($variables['element']['#field_name'])) {
      switch ($variables['element']['#field_name']) {
        case 'field_paragraphs':
          $suggestions[] = 'field__bare';
          break;

        case 'field_related_content':
          // @todo We should use a custom field formatter instead.
          if ($variables['element']['#bundle'] == 'page') {
            // @todo 'featured' doesn't necessarily mean it's a card.
            if (isset($variables['element'][0]) && $variables['element'][0]['#view_mode'] == 'featured') {
              $suggestions[] = 'field__card_container';
            }
          }
          break;
      }
    }
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter() for viewfields.
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_theme_suggestions_viewfield_alter(array &$suggestions, array $variables) {
// @codingStandardsIgnoreEnd
  $suggestions[] = 'field__bare';
}

/**
 * Implements hook_preprocess_field().
 */
// @codingStandardsIgnoreStart
function RADIX_SUBTHEME_MACHINE_NAME_preprocess_field(array &$variables) {
// @codingStandardsIgnoreEnd
  $element = $variables['element'];

  switch ($variables['field_name']) {
    case 'field_type':
    case 'field_tags':
      $variables['utilities'][] = 'metadata';
      break;

    case 'field_pg_cta':
      // We want the classes on the <a> tag, which does not accept an Attribute
      // object the way the field itself does.
      foreach ($variables['items'] as $delta => $item) {
        $btn_style = 'btn-primary';
        if ($delta > 0) {
          $variables['field_wrapper'] = 'span';
          $btn_style = 'btn-secondary';
        }
        $variables['items'][$delta]['content']['#options']['attributes'] = [
          'class' => [
            'btn',
            $btn_style,
            'btn-lg',
          ],
        ];
      }
      break;

    case 'field_related_content':
      $variables['title_wrapper'] = 'h2';
      break;

    case 'dynamic_block_field:node-breadcrumbs':
      $variables['utilities'][] = 'container';
      break;
  }

  switch ($variables['field_type']) {
    // If a datetime fieldname ends with '_start', look for a corresponding
    // field ending in '_end' and pass it as a renderable array to a template.
    // This is an ugly work around for lack of a stable daterange field type.
    case 'datetime':
      $i = strpos($variables['field_name'], '_start');
      if ($i === FALSE) {
        break;
      }
      $end_date_field = substr($variables['field_name'], 0, $i) . '_end';
      $end_date_items = [];
      if ($element['#object']->hasField($end_date_field) == FALSE) {
        break;
      }
      // The end date must be enabled on the same view mode as the start date
      // (it can be in the hidden region) and should be using the same format.
      $end_date = $element['#object']->$end_date_field->view($element['#view_mode']);
      if (!isset($end_date['#field_type'])
        || $end_date['#field_type'] !== 'datetime'
        || !isset($end_date['#view_mode'])
        || $end_date['#view_mode'] !== $element['#view_mode']) {
        break;
      }
      foreach ($end_date as $key => $item) {
        if (is_int($key)) {
          // Separator.
          $start = explode(' at ', $element[$key]['#text']);
          $end = explode(' at ', $item['#text']);
          if ($start[0] == $end[0]) {
            if (!isset($end[1])) {
              break;
            }
            else {
              $item['#text'] = $end[1];
            }
          }
          $end_date_items[$key]['content'] = $item;
        }
      }
      $variables['end_date_items'] = $end_date_items;
      break;
  }
}
