/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your application. See https://github.com/JeffreyWay/laravel-mix.
 |
 */
const proxy = "http://drupal.local";
const mix = require("laravel-mix");
const globImporter = require('node-sass-glob-importer');

require("laravel-mix-imagemin");

/*
  |--------------------------------------------------------------------------
  | Configuration
  |--------------------------------------------------------------------------
  */
mix
  .webpackConfig({
    // Use the jQuery that ships with Drupal to avoid conflicts
    externals: {
      jquery: "jQuery",
    },
    module: {
      rules: [
        {
          test: /RADIX_SUBTHEME_MACHINE_NAME\.style\.scss/,
          loader: "import-glob-loader"
        },
        {
          // We need this for the postcss plugin to work below,
          // but we don't want to use it for our main styles
          // because it breaks source maps.
          test: /ckeditor5\.style\.scss/,
          use: [
            {
              loader: 'sass-loader',
              options: {
                sassOptions: {
                  importer: globImporter()
                }
              }

            }
          ]
        },
      ]
    },
  })
  .setPublicPath("assets")
  .options({
    processCssUrls: false,
    cssNano: { minifyFontValues: false },
  });

/*
  |--------------------------------------------------------------------------
  | Generate Source Maps
  |--------------------------------------------------------------------------
  */

mix.sourceMaps(false, "source-map");

/*
  |--------------------------------------------------------------------------
  | Fonts
  |--------------------------------------------------------------------------
  */

mix.copy("src/fonts/**", "assets/fonts");

mix.copy("src/images/**", "assets/images");

/*
  |--------------------------------------------------------------------------
  | Browsersync
  |--------------------------------------------------------------------------
  */
mix.browserSync({
  proxy: proxy,
  files: ["assets/js/**/*.js", "assets/css/**/*.css"],
  stream: true,
});

/*
  |--------------------------------------------------------------------------
  | SASS
  |--------------------------------------------------------------------------
  */
mix.sass("src/sass/RADIX_SUBTHEME_MACHINE_NAME.style.scss", "css");

// Isolate modified styles for CKEditor to prevent leaking.
mix.sass("src/sass/ckeditor5.style.scss", "css", {}, [
  require("postcss-selector-replace")({
    before: [".ck-content body", ".ck-content html"],
    after: [".ck-content", ".ck-content"],
  }),
]);

/*
  |--------------------------------------------------------------------------
  | JS
  |--------------------------------------------------------------------------
  */
mix.js("src/js/RADIX_SUBTHEME_MACHINE_NAME.script.js", "js");
